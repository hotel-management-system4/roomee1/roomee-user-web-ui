import React, { useReducer, createContext } from 'react';
import AuthReducer from '../reducer/authReducer';

const InitialState = 'Un Signed in';
// const InitialState = 'Signed in';

const AuthContext = createContext<{
  authState: string;
  dispatch: React.Dispatch<any>;
}>({
  authState: InitialState,
  dispatch: () => null,
});

function AuthProvider({ children }: { children?: React.ReactNode }) {
  const [authState, dispatch] = useReducer(AuthReducer, InitialState);
  return (
    <AuthContext.Provider value={{ authState, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
}

export { AuthContext, AuthProvider };
