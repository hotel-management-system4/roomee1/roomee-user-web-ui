import { AuthConst } from '../../const';

interface Action {
  type: string;
  payload: any;
}

export default function AuthReducer(state: string, action: Action) {
  const { type, payload } = action;
  switch (type) {
    case AuthConst.LOGIN:
      return state;
    case AuthConst.LOG_OUT:
      return state;
    default:
      console.log(payload);
      return state;
  }
}
