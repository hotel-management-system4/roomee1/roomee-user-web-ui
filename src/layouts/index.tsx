import { Outlet } from 'react-router-dom';

export function UnSignedInLayout() {
  return (
    <>
      <Outlet />
    </>
  );
}

export function SignedInLayout() {
  return (
    <>
      <Outlet />
    </>
  );
}
