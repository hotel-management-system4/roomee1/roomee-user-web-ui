// User model
export interface UserLoginModel {
  email: string
  password: string
}
