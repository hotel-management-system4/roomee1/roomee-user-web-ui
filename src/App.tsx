import { Route, Routes } from 'react-router-dom';
import LoginPage from './pages/auth/LoginPage/LoginPage';
import ProtectedRoute from './routes/ProtectedRoute';
import UnProtectedRoute from './routes/UnProtectedRoute';

export default function App() {
  return (
    <Routes>
      <Route element={<ProtectedRoute />}>
        <Route index element={<h1>HOME PAGE</h1>} />
      </Route>
      <Route element={<UnProtectedRoute />}>
        <Route path="/login" element={<LoginPage />} />
      </Route>
    </Routes>
  );
}