import { useContext } from 'react';
import { Navigate, useLocation } from 'react-router-dom';

import { UnSignedInLayout } from '../layouts';
import { AuthContext } from '../stores/context';

export default function UnProtectedRoute() {
  const { authState } = useContext(AuthContext);
  const location = useLocation();

  if (authState === 'Signed in') {
    return <Navigate to="/" state={{ from: location }} />;
  }

  return (
    <>
      <UnSignedInLayout />
    </>
  );
}
