import { useContext } from 'react';
import { useLocation, Navigate } from 'react-router-dom';

import { SignedInLayout } from '../layouts';
import { AuthContext } from '../stores/context';

export default function ProtectedRoute() {
  const { authState } = useContext(AuthContext);
  const location = useLocation();

  if (authState !== 'Signed in') {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return (
    <>
      <SignedInLayout />
    </>
  );
}
