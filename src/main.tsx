import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import { BrowserRouter } from 'react-router-dom';
import Compose from './stores/index.tsx';
import { AuthProvider } from './stores/provider/authProvider.tsx';

const root = ReactDOM.createRoot(document.getElementById('root')!);
root.render(
  <Compose components={[BrowserRouter, AuthProvider]}>
    <App />
  </Compose>
);
