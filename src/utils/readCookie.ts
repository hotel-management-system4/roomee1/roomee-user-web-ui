export default function readCookie(cookieName: string) {
  const cookies = document.cookie.split(';');
  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i].trim();
    if (cookie.indexOf(cookieName + '=') === 0) {
      const cookieValue = cookie.substring(cookieName.length + 1);
      try {
        console.log('cookie value');
        console.log(cookieValue);
        return JSON.parse(decodeURIComponent(cookieValue));
      } catch (err) {
        console.log('error');
        return null;
      }
    }
  }
  return null;
}
