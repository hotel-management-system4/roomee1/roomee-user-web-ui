import { useContext } from 'react';
import { AuthContext } from '@context';

export default function LoginPage() {
  const { authState } = useContext(AuthContext);
  console.log(authState);
  return (
    <>
      <h1>LOGIN PAGE</h1>
    </>
  );
}
